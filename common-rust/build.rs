fn main() {
    // tell rustc to link with the lib32BlitC.a library
    println!("cargo:rustc-link=32BlitC");

    // for some reason `cfg!(target_os = "none")` does not work (is not true)
    let is_pico_build = "none" == std::env::var("CARGO_CFG_TARGET_OS").unwrap();
    let blitc_build_dir = if is_pico_build { "../build.pico" } else { "../build" };
    // search the corresponding directory for that library depending on the target
    println!("cargo:rustc-link-search={}/32blitc/", blitc_build_dir);
}
