// remove dependency on libstd if building for Picosystem
#![cfg_attr(all(target_arch = "arm", target_os = "none"), no_std)]

#[cfg(all(target_arch = "arm", target_os = "none"))]
pub mod panic;

#[repr(C)]
pub enum ScreenMode { Lores, Hires, HiresPalette }

#[link(name = "32BlitC", kind = "static")]  // link against libBlitEngine.a included in build.rs
extern "C" {
    fn blit_set_screen_mode(new_mode: ScreenMode);
}

#[no_mangle]
pub extern fn rs_set_screen_mode(new_mode: ScreenMode) {
    unsafe { blit_set_screen_mode(new_mode); }
}
