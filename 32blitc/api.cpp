#include <stdint.h>
#include "engine/engine.hpp"

extern "C" void blit_set_screen_mode(blit::ScreenMode new_mode) {
    set_screen_mode(new_mode);
}
