# 32Blit **Rust** Boilerplate

Forked from [32blit-boilerplate](https://github.com/32blit/32blit-boilerplate).

This is a basic template for starting 32blit projects using the Rust language.
It shows the basic code layout and asset pipeline, hopefully giving folk a
starting point for any new projects.

It's based on the original `template` project from the 
[32Blit Beta](https://github.com/pimoroni/32blit-beta), with added asset
handling, and some tidying up to fit in with how I do things.

## Usage

[Use this template](https://github.com/32blit/32blit-boilerplate/generate) to
generate your own project.

* Edit the CMakeList.txt file to set the name of your project
* Edit the metadata.yml file to set the information for your project
* Edit the LICENSE file to set your name on the license
* Write lots of super cool code!

You should then be able to follow the usual build instructions.

For local builds this is:
```
mkdir build
cd build
cmake -D32BLIT_DIR=/path/to/32blit-sdk/ ..
```

Platform/Editor specific insctuctions [can be found in the main 32blit repo](https://github.com/pimoroni/32blit-beta#more-docs)
(For Visual Studio, you should follow the "Option 2" instructions, as the boilerplate does not contain a solution file)

## Dependencies (Fedora)

```bash
sudo dnf install \
git \
gcc g++ \
cmake \
make \
python3 python3-pip python3-setuptools \
unzip

cd ..  # considering being in Super-Square-Bros/ directory
git clone https://github.com/32blit/32blit-sdk
git clone https://github.com/raspberrypi/pico-sdk --recursive
git clone https://github.com/raspberrypi/pico-extras --recursive
cd Super-Square-Bros/

python3 -m pip install 32blit
export PATH=$PATH:~/.local/bin
```
For further system specific requirements see down below.

## Building

### PicoSystem

Rust toolchain:
```
rustup target add thumbv6m-none-eabi
```

#### System Specific Requirements

Fedora:
```bash
sudo dnf install -y \
arm-none-eabi-gcc-cs arm-none-eabi-gcc-cs-c++ arm-none-eabi-newlib
```
Ubuntu:
```bash
sudo apt install gcc-arm-none-eabi
```

#### Actual building
```bash
export PATH=$PATH:~/.local/bin
mkdir -p build.pico
cd build.pico
cmake .. -DCMAKE_TOOLCHAIN_FILE=../../32blit-sdk/pico.toolchain -DPICO_BOARD=pimoroni_picosystem
make
```

### Linux

#### System Specific Requirements

Fedora:
```bash
sudo dnf install -y SDL2-devel SDL2_image-devel SDL2_net-devel
```
Ubuntu:
```bash
sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-net-dev
```

#### Actual building:
```bash
export PATH=$PATH:~/.local/bin
mkdir -p build
cd build
cmake ..
make
```
